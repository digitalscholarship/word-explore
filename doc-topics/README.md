# Creative Tables
This `README` explains the steps to extract keywords from a document topics matrix. In this example, we are looking for the keywords `creative` and `creativeness`, but one may replace these terms with any word by performing some simple tweaks to the steps.
# Overview

The tables `creative_topics.csv` and `creativeness_topics.csv` include Document
ID and topics are at least 95%-ile in probability. Note that the Doc Topics ID and Info ID differ by 1 because indexing in the doc topics matrix starts at 0, while the latter starts at 1.

* `creative_topics.csv` - Top topics for the keyword `creative`
* `creativeness_topics.csv` - Top topics for the keyword `creativeness`
* `top_topics.csv` - Contains the top topics across all documents from the doc-topics matrix.
* `./grep_output` - contains the output from grep (`.txt` files) and the extracted row numbers in RDS format.

# Obtaining the Data

1. Perform `grep` on the cleaned corpus. In this case, the cleaned corpus is `info.dat`. Use the `-n` parameter to return the line numbers of where the match is found. The `-E` parameter means extended regex, which gives us more features to perform the regex. Replace `<KEYWORD>` with your keyword. This should take less than a minute.
``` 
grep -n -E "\b<KEYWORD>\b" > output.dat 
```

2. This step is only necessary to get the top topics from the doc-topics matrix. Currently, the data is stored in `top_topics.csv` and was obtained by running `cut_tail.R`. `cut_tail.R` goes through each row of the doc topics matrix and finds the top topics of the document, and stores the output line-by-line in CSV format.

3. Extract the row numbers from the grep output. This can be done by using `get_row_numbers.R`.



4. Run `extract_topics.R` on the extracted row numbers. This produces a data frame where the `ID` column is the ID from `info.dat` and the top topics from `top_topics.csv`. 
<!--stackedit_data:
eyJoaXN0b3J5IjpbMTI5MDExNDcxOSwtMTEzMTE4MzU2MCwxND
c0NzMyMjU3XX0=
-->