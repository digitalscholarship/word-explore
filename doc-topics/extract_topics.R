# Subsets the top topics matrix based on the indices of keywords
# For now, the keywords are creative and creativeness
# The row number data will be read in as a character vector, so we will
# need to cast it to numeric

# Read in the row numbers of the creative keywords
creative <- as.numeric(as.character(readRDS("keywords/creative_idx_only.RDS")))

# Read in the row numbers of the creativeness keywords
creativeness <- as.numeric(as.character(readRDS("keywords/creativeness_idx_only.RDS")))

# Reada in the top topics
topics <- read.csv("top_topics.csv")

# Index the dataframes based on the indices of documents
# Containing the keyword creative

# Get tne document IDs containing the keyword
creative_IDs <- topics$ID[creative]

# Get tne document topics containing the keyword
creative_topics <- topics$Topics[creative]

# Containing the keyword creativeness
creativeness_IDs <- topics$ID[creativeness]
creativeness_topics <- topics$Topics[creativeness]

# Make the data frames to save to file
creative_df <- data.frame(ID=creative_IDs, 
			topics = creative_topics)

creativeness_df <- data.frame(ID=creativeness_IDs, 
			topics = creativeness_topics)


# Save the subsetted data frames
# row.names = F makes it so the row numbers aren't written
# in a separate column to file
write.csv(creative_df, "creative_df/creative_topics.csv", row.names=F)
write.csv(creativeness_df, "creative_df/creativeness_topics.csv", row.names=F)