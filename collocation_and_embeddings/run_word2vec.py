import os
import time
import gensim
import multiprocessing
import joblib
from joblib import Parallel, delayed

class MySentences(object):
    def __init__(self, fname):
        self.fname = fname

    def __iter__(self):
        for line in open(os.path.join(self.fname)):
            yield line.split()


def compute_embeddings(ifile):
    odir = "/dsl/David_Kyle_Creativeness/24_01_2019/embeddings/"
    temp = time.time()
    date = ifile[-4:]
    s_iter = MySentences (ifile)
    model = gensim.models.Word2Vec(s_iter, sg=1, window=15, size=100, workers = 4)
    model.save(odir + date + ".model")
    print(ifile, " saved in ", time.time() - temp)

def main():
    subsetdir = "/dsl/David_Kyle_Creativeness/24_01_2019/subsets/"
    ifiles = os.listdir(subsetdir)
    ifiles.sort()
    ifiles = [subsetdir + i for i in ifiles]
    
    
    start = time.time()
    print("running all models")

    Parallel(n_jobs=30)(delayed(compute_embeddings)(ifile) for ifile in ifiles)
    print("processed in ", (time.time() - start) / 60, " minutes")

main()
