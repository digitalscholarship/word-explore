import os
import nltk.collocations
import collections
import nltk.corpus
import multiprocessing
import joblib
from joblib import Parallel, delayed
import time

class MyWords(object):
    def __init__(self, fname):
        self.fname = fname

    def __iter__(self):
        for line in open(os.path.join(self.fname)):
            for w in line.split():
                yield w

def compute_collocations(ifile):
    odir = "/dsl/David_Kyle_Creativeness/24_01_2019/collocations/"
    ofile = open(odir + ifile[-4:], "w")
    temp = time.time()
    words = MyWords(ifile) # create generator for words from file

    bgm = nltk.collocations.BigramAssocMeasures()
    finder = nltk.collocations.BigramCollocationFinder.from_words(words)
    scored = finder.score_ngrams(bgm.likelihood_ratio)

    # https://stackoverflow.com/questions/8683588/understanding-nltk-collocation-scoring-for-bigrams-and-trigrams
    # Group bigrams by first word in bigram.                                        
    prefix_keys = collections.defaultdict(list)
    for key, scores in scored:
       prefix_keys[key[0]].append((key[1], scores))

   # Sort keyed bigrams by strongest association.                                  
    for key in prefix_keys:
       prefix_keys[key].sort(key = lambda x: -x[1])

    for k,v in prefix_keys.items():
        print(k, v, file=ofile)

    print(ifile, " saved in ", time.time() - temp)

def main():
    subsetdir = "/dsl/David_Kyle_Creativeness/24_01_2019/subsets/"
    ifiles = os.listdir(subsetdir)
    ifiles.sort()
    ifiles = [subsetdir + i for i in ifiles]

    start = time.time()
    print("running all collocations")
    Parallel(n_jobs=70)(delayed(compute_collocations)(ifile) for ifile in ifiles)
    print("processed in ", (time.time() - start) / 60, " minutes")

if __name__ == "__main__":
    main()
