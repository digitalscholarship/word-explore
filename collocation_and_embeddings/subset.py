import regex as re
from string import digits
from string import punctuation

udates = "/dsl/David_Kyle_Creativeness/24_01_2019/count_dates.dat"
dfile = "/dsl/David_Kyle_Creativeness/24_01_2019/dates.dat"
sentences = "/dsl/David_Kyle_Creativeness/24_01_2019/corpus.dat"

fnames = {}
with open(udates, "r") as uniquedates:
    for line in uniquedates:
        line = line.rstrip()
        elem = line.split()
        date = elem[0]
        fnames[date] = "/dsl/David_Kyle_Creativeness/24_01_2019/subsets/" + date

i = 0
ROMAN = r"^(?=[MDCLXVI])M*(C[MD]|D?C{0,3})(X[CL]|L?X{0,3})(I[XV]|V?I{0,3})$"
with open(sentences, "r") as corpus, open(dfile, "r") as dates:
    for line,date in zip(corpus,dates):
        i = i + 1
        if i == 1:
            continue
            
        line = line.rstrip()
        line = line.lower()
        date = date.rstrip()

        line = line.translate(str.maketrans('','',".?!#[]()$%&@:;,"))
        line = re.sub(ROMAN, "", line, flags=re.IGNORECASE)

        elem = line.split()
        res = ""
        for e in elem:
            if len(e) > 2 and "-" not in e:
                res = res + " " + e
            elif len(e) > 5 and "-" in e:
                res = res + " " + e

        if res != "":
            print(res, file=open(fnames[date], "a"))


