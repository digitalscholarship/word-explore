ifile = "/dsl/David_Kyle_Creativeness/24_01_2019/dates.dat"
ofile = "/dsl/David_Kyle_Creativeness/24_01_2019/count_dates.dat"

unique = {}
with open(ifile,"r") as dates:
    for line in dates:
        line = line.strip()
        if line not in unique:
            unique[line] = 0
        unique[line] += 1

with open(ofile,"w") as output:
    for k,v in unique.items():
        print(k, v, file=output)
