import os
import gensim
from gensim.models import Word2Vec
# given input directory
# load each model and print the top 10 nearest neighbors and their score
# according to word2vec

def get_nn(idir, modelname, word):
    if modelname[0] != '1':
        return
    date = modelname[0:4]
    model = Word2Vec.load(idir + modelname)
    wv = model.wv
    vocab = wv.vocab.keys()
    if word in vocab:
        nn = wv.most_similar(word)
        for n in nn:
            print(date + ',' + "creative" + ',' + n[0] + ',' + str(n[1]))

def main():
    idir = "/dsl/David_Kyle_Creativeness/24_01_2019/embeddings/"
    filelist = os.listdir(idir)
    filelist.sort()

    for f in filelist:
        get_nn(idir, f, "creative")

main()
