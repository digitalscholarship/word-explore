#first grep "^creative" * > collocations_creative.dat

ifilepath = "/dsl/David_Kyle_Creativeness/24_01_2019/collocations/collocations_creative.dat"

with open(ifilepath, "r") as ifile:
    for line in ifile:
        line = line.rstrip()
        ws = line.split()
        date = ws[0].split(':')[0]
        word = ws[0].split(':')[1]
        rest = " ".join(ws[1:])
        rest = rest.replace('[','')
        rest = rest.replace(']','')
        rest = rest.replace('(','')
        rest = rest.replace(')','')
        rest = rest.replace("',",'')
        rest = rest.replace("'",'')
        bigrams = rest.split(',')
        start = date + "," + word
        for b in bigrams:
            elem = b.split()
            gram = elem[0]
            score = elem[1]
            print(start + "," + gram + "," + score)
            
        

